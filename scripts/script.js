//variables to keep track of the different css files we'll be swapping in and out
const blueTheme = "botui-theme-blue";
const greenTheme = "botui-theme-green";
const redTheme = "botui-theme-red";

//--BotUI--//
var botui = new BotUI('my-botui-app');

//Called once the page loads
window.onload = function(){
    //enable just the default stylesheet
    disableAllStyleSheets();

    //setup modal
    setupModal();

    //start the first message
    startMessage();
}

//01 Message
var startMessage = function(){
    botui.message.add({
    type: 'html',
    content: 'This message has its type set to `html`. I can add inline elements like <span id="myLink" class="clickable">this</span>.' //add an inline link that doesn't actually link anywhere, but it has an id we can use
  }).then(function(){
    //add an event handler to the span element to open the modal
    //we'll pass the id of the element to the function so we can look up what values to use in the modal
    createEventListenerForId("myLink");

    //goto 01 Action
    askTheme();
  });
}

//01 Action
var askTheme = function(){
    return botui.action.button({
        delay: 1000,
        action: [{
            text: "I'm feeling blue",
            value: blueTheme
        },{
            text: "Green means go!",
            value: greenTheme
        },{
            text: "Always bet on red",
            value: redTheme
        }]
    }).then(function(res){
        //change active stylesheet based on input
        disableAllStyleSheets();
        enableStyleSheet(res.value);

        //goto 02 Message
        displayTheme();
    });
}

//02 Message
var displayTheme = function(){
    botui.message.add({
        content: "Nice! How do I look?"
    }).then(function(){
        //goto 01 Action
        askTheme();
    });
}

//--CSS manipulation--//
//The following funcitons allow us to disable and enable stylesheets
function disableAllStyleSheets()
{
    disableStyleSheet(blueTheme);
    disableStyleSheet(greenTheme);
    disableStyleSheet(redTheme);
}

function disableStyleSheet(sheetName)
{
    var stylesheet = document.querySelector('link[href*=' + sheetName + ']')
    stylesheet.disabled = true;
}

function enableStyleSheet(sheetName)
{
    var stylesheet = document.querySelector('link[href*=' + sheetName + ']')
    stylesheet.disabled = false;
}


//---MODALS---//
//setup modal
const modal = document.getElementById("modal-container");
const modalTitle = document.getElementById("modal-title");
const modalText = document.getElementById("modal-text");
function setupModal()
{
    modal.classList.add("hidden");
    //this event listener will hide the model once it is clicked on
    modal.addEventListener("click", function(){
        modal.classList.add("hidden");
    })
}

//Add event listener to elements in botui messages
function createEventListenerForId(id)
{
    var link = document.getElementById(id);
    link.addEventListener("click", displayModal);
};


function displayModal(e){
    //update text in modal by looking up values from the glossary map
    modalTitle.innerHTML = glossary.get(this.id).title;
    modalText.innerHTML = glossary.get(this.id).text;
    modal.classList.remove("hidden");
}

//Create a map to store all your definitions
//Use the element id as the 'key'
let glossary = new Map();
glossary.set('myLink', {title: "Name of the thing", text: "This is the definition of the thing."});
glossary.set('anotherLink', {title: "Name of the other thing", text: "This is the definition of the other thing."});
//...and so on

